# Argocd Tests

kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
kubectl apply -f application.yml

password=$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo)

kubectl port-forward svc/argocd-server -n argocd 8080:443

http://localhost:8080
admin
echo $password

# istioctl

curl -L https://istio.io/downloadIstio | sh -

export PATH="$PATH:/Users/mc/istio-$VERSION/bin"

istioctl install -y

kubectl get ns default --show-labels
kubectl label namespace default istio-injection=enabled #use for enable istio for namespace